ft2-clone (1.86+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/control: drop transitional package.

 -- Alex Myczko <tar@debian.org>  Fri, 13 Sep 2024 11:14:19 +0000

ft2-clone (1.85+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: updated Files-Excluded.

 -- Alex Myczko <tar@debian.org>  Mon, 02 Sep 2024 13:55:16 +0000

ft2-clone (1.84+ds-1) unstable; urgency=medium

  * New upstream version.

 -- Alex Myczko <tar@debian.org>  Mon, 13 May 2024 14:26:13 +0000

ft2-clone (1.80+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: bump years.

 -- Alex Myczko <tar@debian.org>  Tue, 09 Apr 2024 16:08:03 +0200

ft2-clone (1.78+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: bump years.

 -- Gürkan Myczko <tar@debian.org>  Mon, 18 Mar 2024 06:27:10 +0100

ft2-clone (1.72.1+ds-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.6.2.
  * d/copyright: bump copyright year.

 -- Gürkan Myczko <tar@debian.org>  Mon, 16 Oct 2023 19:41:59 +0200

ft2-clone (1.62+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: add Files-Excluded, bump copyright year.

 -- Gürkan Myczko <tar@debian.org>  Tue, 03 Jan 2023 14:44:16 +0100

ft2-clone (1.54+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/control: update Vcs URL.

 -- Gürkan Myczko <tar@debian.org>  Fri, 22 Apr 2022 18:48:36 +0200

ft2-clone (1.53+ds-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Mon, 18 Apr 2022 20:25:47 +0200

ft2-clone (1.50+ds-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.6.0.
  * Update maintainer address.
  * d/control: Section: prefix oldlibs with non-free.
  * d/watch: mangle repackaging.
  * d/copyright: update copyright years and add more authors.

 -- Gürkan Myczko <tar@debian.org>  Tue, 18 Jan 2022 19:39:35 +0100

ft2-clone (1.47+ds-1) unstable; urgency=medium

  * New upstream version.
  * Rename source and binary package to match upstream.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 09 Sep 2021 14:26:47 +0200

fasttracker2 (1.41+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright:
    - set Upstream-Contact, fix Upstream-Name.
    - update years.
  * Bump debhelper version to 13, drop d/compat.
  * d/upstream/metadata: added.
  * d/control: added Rules-Requires-Root.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 01 Dec 2020 17:12:55 +0100

fasttracker2 (1.24-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 24 May 2020 16:06:02 +0200

fasttracker2 (1.22-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 28 Apr 2020 10:59:31 +0200

fasttracker2 (1.19-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 12 Apr 2020 15:28:36 +0200

fasttracker2 (1.15-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 01 Apr 2020 11:37:14 +0200

fasttracker2 (1.14-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 23 Mar 2020 14:47:38 +0100

fasttracker2 (1.12-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 17 Mar 2020 16:43:52 +0100

fasttracker2 (1.09-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.5.0.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 13 Feb 2020 10:19:57 +0100

fasttracker2 (1.07-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 31 Jan 2020 10:04:42 +0100

fasttracker2 (1.06-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 15 Jan 2020 13:52:03 +0100

fasttracker2 (1.05-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 03 Jan 2020 08:19:34 +0100

fasttracker2 (1.04-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: update copyright years, new source url.
  * d/watch: added.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 20 Dec 2019 09:23:20 +0100

fasttracker2 (1.03-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 29 Nov 2019 13:27:31 +0100

fasttracker2 (1.02-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.4.1.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 25 Nov 2019 11:05:29 +0100

fasttracker2 (1.00-1) unstable; urgency=medium

  * New upstream version. (Closes: #922026)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 01 Oct 2019 09:32:45 +0200

fasttracker2 (0.1+b168-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.4.0.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 06 Sep 2019 11:44:12 +0200

fasttracker2 (0.1+b166-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 14 Aug 2019 16:45:34 +0200

fasttracker2 (0.1+b164-1) unstable; urgency=medium

  * New upstream version.
  * debian/control: build on any architecture.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 05 Aug 2019 17:10:52 +0200

fasttracker2 (0.1+b163-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 29 Jul 2019 17:49:58 +0200

fasttracker2 (0.1+b161-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 04 Jul 2019 22:09:11 +0200

fasttracker2 (0.1+b156-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 27 May 2019 12:21:42 +0200

fasttracker2 (0.1+b146-1) experimental; urgency=medium

  * New upstream version.
  * Bump standards version to 4.3.0.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 08 Apr 2019 14:05:28 +0200

fasttracker2 (0.1+b144-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sat, 30 Mar 2019 13:48:39 +0100

fasttracker2 (0.1+b142-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 20 Mar 2019 08:05:41 +0100

fasttracker2 (0.1+b140-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 11 Mar 2019 07:59:54 +0100

fasttracker2 (0.1+b139-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 07 Mar 2019 05:58:08 +0100

fasttracker2 (0.1+b137-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 15 Feb 2019 08:21:19 +0100

fasttracker2 (0.1+b134-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 11 Feb 2019 16:32:36 +0100

fasttracker2 (0.1+b132-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 31 Jan 2019 13:28:10 +0100

fasttracker2 (0.1+b130-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 25 Jan 2019 15:18:37 +0100

fasttracker2 (0.1+b126-1) experimental; urgency=medium

  * New upstream version.
  * debian/control: add to Multimedia Team.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 30 Dec 2018 13:34:21 +0100

fasttracker2 (0.1+b125-1) experimental; urgency=medium

  * New upstream version.
  * debian/control: don't build on some archs. (Closes: #914981)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 28 Dec 2018 17:02:49 +0100

fasttracker2 (0.1+b120-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 07 Dec 2018 09:20:51 +0100

fasttracker2 (0.1+b115-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 16 Nov 2018 13:55:39 +0100

fasttracker2 (0.1+b109-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 12 Oct 2018 16:39:50 +0200

fasttracker2 (0.1+b104-1) experimental; urgency=medium

  * New upstream version.
  * debian/control:
    - don't list architectures. (Closes: #909199)
      reason to have it expanded was, i386 would not build without -msse2
  * debian/copyright: remove double entry (rtmidi).

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 28 Sep 2018 10:19:46 +0200

fasttracker2 (0.1+b96-1) experimental; urgency=medium

  * Initial release. (Closes: #907560)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 11 Sep 2018 22:27:10 +0200
